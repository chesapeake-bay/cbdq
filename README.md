# Chesapeake Bay Data Quadrant #



### What is Chesapeake Bay Data Quadrant? ###

**Chesapeake Bay Data Quadrant (CBDQ)** is a data science project that supports **Chesapeake Bay** conservation 
by providing four interactive environmental & water quality reports.

**CBDQ** contributes to the **Chesapeake Monitoring Cooperative (CMC)** water quality data initiative and 
consists of the following four components: 

* Water Quality Restoration Visualization
* GIS Data Gap Map
* Water Pollution Analysis
* Water Quality Report Card